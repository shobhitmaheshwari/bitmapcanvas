package com.example.shobhit.bitmapcanvas;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by shobhit on 3/3/16.
 */
public class CanvasView extends View {

    Paint paint;
    Bitmap bitmap;
    Location location;

    class Location {
        float x;
        float y;
        void setLocation(float a, float b){x = a; y = b;}
    }

    public CanvasView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.BLUE);

        //get bitmap of the resource
        bitmap = BitmapProcess.decodeSampledBitmapFromResource(
                getResources(), R.drawable.circle, 80, 80);
        //top left corner
        location = new Location();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //offset by the width of bitmap
        canvas.drawBitmap(bitmap, location.x-bitmap.getWidth()/2,
                location.y-bitmap.getHeight()/2, paint);
    }


    private void moveCircle(float x, float y) {
        location.x = x;
        location.y = y;
    }

    //override the onTouchEvent
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean handled = false;
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                handled = true;
                break;
            case MotionEvent.ACTION_MOVE:
                //I am only handling the case when the object is moved.
                moveCircle(x, y);
                invalidate();
                handled = true;
                break;
            case MotionEvent.ACTION_UP:
                handled = true;
                break;
            case MotionEvent.ACTION_CANCEL:
                handled = true;
                break;

            default:
                // do nothing
                break;
        }
        return super.onTouchEvent(event) || handled;
    }

    @Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld){
        super.onSizeChanged(xNew, yNew, xOld, yOld);
        location.setLocation(xNew/2, yNew/2);
    }
}